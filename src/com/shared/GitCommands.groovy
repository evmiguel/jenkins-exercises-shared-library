#!/user/bin/env groovy

package com.shared

class GitCommands implements Serializable {
    def script

    GitCommands(script) {
        this.script = script
    }

    def commitNewVersion(String repoUrl, String branchName) {
        script.withCredentials([script.usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]){
            script.sh 'git config --global user.email "jenkins@example.com"'
            script.sh 'git config --global user.name "jenkins"'

            script.sh 'git status'
            script.sh 'git branch'
            script.sh 'git config --list'

            script.sh "git remote set-url origin https://${script.USER}:${script.PASS}@$repoUrl"
            script.sh 'git add .'
            script.sh 'git commit -m "ci: version bump"'
            script.sh "git push origin HEAD:$branchName"
        }
    }
}