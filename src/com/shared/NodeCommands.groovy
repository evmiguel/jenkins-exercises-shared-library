#!/user/bin/env groovy

package com.shared

class NodeCommands implements Serializable {
    def script

    NodeCommands(script) {
        this.script = script
    }

    def incrementVersion() {
        script.echo "Incrementing version..."
        script.sh "npm version patch"
        def packageProps = script.readJSON file: 'package.json'
        def version = packageProps.version
        script.env.IMAGE_NAME = "$version-${script.BUILD_NUMBER}"
    }

    def runTests() {
        script.echo "Running tests..."
        script.sh "npm install"
        script.sh "npm run test"
    }

}

