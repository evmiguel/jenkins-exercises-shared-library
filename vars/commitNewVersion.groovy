#!/user/bin/env groovy

import com.shared.GitCommands
def call(String repoUrl, String branchName) {
    return new GitCommands(this).commitNewVersion(repoUrl, branchName)
}
