#!/user/bin/env groovy

import com.shared.NodeCommands

def call() {
    return new NodeCommands(this).runTests()
}