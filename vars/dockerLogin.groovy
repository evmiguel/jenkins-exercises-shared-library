#!/user/bin/env groovy

import com.shared.Docker
def call() {
    return new Docker(this).dockerLogin()
}
