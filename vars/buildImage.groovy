#!/user/bin/env groovy

import com.shared.Docker
def call(String imageName) {
    return new Docker(this).buildImage(imageName)
}
